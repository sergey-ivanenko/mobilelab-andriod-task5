package org.bitbucket.sergey_ivanenko.task5.common

import com.google.android.gms.maps.model.LatLng

object Constants {
    const val BASE_URL = "https://belarusbank.by/api/"
    const val CURRENT_CITY = "Гомель"
    const val SHOW_PLACE_COUNT = 10
    const val MAP_ZOOM = 14.8f
    val CURRENT_PLACE = LatLng(52.425163, 31.015039)
}