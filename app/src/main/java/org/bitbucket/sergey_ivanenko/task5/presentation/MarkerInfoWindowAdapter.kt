package org.bitbucket.sergey_ivanenko.task5.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import org.bitbucket.sergey_ivanenko.task5.databinding.MarkerInfoContentsBinding
import org.bitbucket.sergey_ivanenko.task5.domain.model.Place

class MarkerInfoWindowAdapter(
    private val context: Context
) : GoogleMap.InfoWindowAdapter {

    private var _binding: MarkerInfoContentsBinding? = null
    private val binding get() = requireNotNull(_binding)

    override fun getInfoContents(marker: Marker): View? {

        val place = marker.tag as? Place ?: return null

        _binding = MarkerInfoContentsBinding.inflate(
            LayoutInflater.from(context)
        )

        binding.apply {
            textViewTitle.text = place.title
            textViewAddress.text = place.snippet
            textViewType.text = place.placeType
        }

        return binding.root
    }

    override fun getInfoWindow(marker: Marker): View? {
        // Return null to indicate that the
        // default window (white bubble) should be used
        return null
    }
}
