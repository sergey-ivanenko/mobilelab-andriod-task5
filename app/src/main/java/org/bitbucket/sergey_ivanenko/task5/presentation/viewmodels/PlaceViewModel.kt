package org.bitbucket.sergey_ivanenko.task5.presentation.viewmodels

import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.bitbucket.sergey_ivanenko.task5.common.Constants
import org.bitbucket.sergey_ivanenko.task5.common.Constants.SHOW_PLACE_COUNT
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.toPlace
import org.bitbucket.sergey_ivanenko.task5.data.repository.PlaceRepository
import org.bitbucket.sergey_ivanenko.task5.domain.model.Place

class PlaceViewModel(private val repository: PlaceRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val _placeList = MutableLiveData<List<Place>>()
    val placeList: LiveData<List<Place>> get() = _placeList

    fun fetchPlaceList(city: String) {
        compositeDisposable.add(
            Single.zip(
                repository.fetchAtmList(city),
                repository.fetchInfoboxList(city),
                repository.fetchFilialList(city)
            ) { atmList, infoboxList, filialList ->
                val list = mutableListOf<Place>()
                atmList.forEach { list.add(it.toPlace()) }
                infoboxList.forEach { list.add(it.toPlace()) }
                filialList.forEach { list.add(it.toPlace()) }
                findDistance(list)
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result ->
                    _placeList.value = result
                }, {
                    Log.e("TAG", "Response not successful")
                    Log.e("TAG", it.message.toString())
                })
        )
    }

    private fun findDistance(listPlace: List<Place>): List<Place> {
        val arr = floatArrayOf(1f)
        val listDist: MutableList<Pair<Float, Place>> = mutableListOf()
        listPlace.forEach { place ->
            Location.distanceBetween(
                Constants.CURRENT_PLACE.latitude, Constants.CURRENT_PLACE.longitude,
                place.latLng.latitude, place.latLng.longitude,
                arr
            )
            listDist.add(Pair(arr[0], place))
        }
        listDist.sortBy { it.first }

        val placeList: MutableList<Place> = mutableListOf()
        for (i in 0 until listDist.size) {
            if (i < SHOW_PLACE_COUNT) {
                listDist[i].second.distance = "${listDist[i].first}m"
                placeList.add(listDist[i].second)
            } else {
                break
            }
        }
        placeList.forEach {
            Log.d("TAG", "$it")
        }

        return placeList
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}