package org.bitbucket.sergey_ivanenko.task5.presentation.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import org.bitbucket.sergey_ivanenko.task5.R
import org.bitbucket.sergey_ivanenko.task5.common.Constants.CURRENT_CITY
import org.bitbucket.sergey_ivanenko.task5.common.PlaceViewModelFactory
import org.bitbucket.sergey_ivanenko.task5.data.repository.PlaceRepository
import org.bitbucket.sergey_ivanenko.task5.databinding.ActivityMapsBinding
import org.bitbucket.sergey_ivanenko.task5.domain.model.Place
import org.bitbucket.sergey_ivanenko.task5.presentation.MarkerInfoWindowAdapter
import org.bitbucket.sergey_ivanenko.task5.presentation.PlaceRenderer
import org.bitbucket.sergey_ivanenko.task5.presentation.viewmodels.PlaceViewModel

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    private val repository = PlaceRepository()
    private val placeViewModel by viewModels<PlaceViewModel> { PlaceViewModelFactory(repository) }

    private var listPlace = emptyList<Place>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        placeViewModel.fetchPlaceList(CURRENT_CITY)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap ->
            placeViewModel.placeList.observe(this) {
                listPlace = it
                onMapReady(googleMap)
            }

            googleMap.setOnMapLoadedCallback {
                val bounds = LatLngBounds.builder()
                listPlace.forEach { bounds.include(it.latLng) }
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 75))
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        //val gomel = LatLng(52.425163, 31.015039)
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, MAP_ZOOM))
        //addMarkers(mMap)

        addClusteredMarkers(mMap)
    }

    private fun addMarkers(googleMap: GoogleMap) {
        listPlace.forEach { place ->
            val marker = googleMap.addMarker(
                MarkerOptions()
                    .position(place.latLng)
            )
            marker?.tag = place
        }
        Log.d("TAG", "addMarkers: $listPlace")
    }

    private fun addClusteredMarkers(googleMap: GoogleMap) {
        val clusterManager = ClusterManager<Place>(this, googleMap)
        clusterManager.renderer =
            PlaceRenderer(
                this,
                googleMap,
                clusterManager
            )

        clusterManager.markerCollection.setInfoWindowAdapter(MarkerInfoWindowAdapter(this))

        clusterManager.addItems(listPlace)
        clusterManager.cluster()

        googleMap.setOnCameraIdleListener {
            clusterManager.markerCollection.markers.forEach { it.alpha = 1.0f }
            clusterManager.clusterMarkerCollection.markers.forEach { it.alpha = 1.0f }

            clusterManager.onCameraIdle()
        }
    }
}