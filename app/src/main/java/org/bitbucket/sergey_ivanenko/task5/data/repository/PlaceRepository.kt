package org.bitbucket.sergey_ivanenko.task5.data.repository

import io.reactivex.Single
import org.bitbucket.sergey_ivanenko.task5.data.remote.api.RetrofitInstance
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.AtmDto
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.FilialDto
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.InfoboxDto

class PlaceRepository {

    fun fetchAtmList(city: String): Single<List<AtmDto>> {
        return RetrofitInstance.api.fetchAtmList(city)
    }

    fun fetchInfoboxList(city: String): Single<List<InfoboxDto>> {
        return RetrofitInstance.api.fetchInfoboxList(city)
    }

    fun fetchFilialList(city: String): Single<List<FilialDto>> {
        return RetrofitInstance.api.fetchFilialList(city)
    }
}