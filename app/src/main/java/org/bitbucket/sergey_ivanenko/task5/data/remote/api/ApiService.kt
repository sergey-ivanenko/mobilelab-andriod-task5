package org.bitbucket.sergey_ivanenko.task5.data.remote.api

import io.reactivex.Single
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.AtmDto
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.FilialDto
import org.bitbucket.sergey_ivanenko.task5.data.remote.dto.InfoboxDto
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("atm")
    fun fetchAtmList(@Query("city") city: String): Single<List<AtmDto>>

    @GET("infobox")
    fun fetchInfoboxList(@Query("city") city: String): Single<List<InfoboxDto>>

    @GET("filials_info")
    fun fetchFilialList(@Query("city") city: String): Single<List<FilialDto>>
}