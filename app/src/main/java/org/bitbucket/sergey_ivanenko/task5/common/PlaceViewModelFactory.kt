package org.bitbucket.sergey_ivanenko.task5.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.bitbucket.sergey_ivanenko.task5.data.repository.PlaceRepository
import org.bitbucket.sergey_ivanenko.task5.presentation.viewmodels.PlaceViewModel

class PlaceViewModelFactory(private val repository: PlaceRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PlaceViewModel(repository) as T
    }
}